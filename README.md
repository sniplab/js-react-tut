## Start App

`yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Testing

### Installation test libraries

- Mocha: https://mochajs.org
- Mocha + Chai: https://www.chaijs.com
  
```
npm install --global mocha
npm install --save-dev mocha
npm install --save-dev chai
# npm install chai
```

if there was any change in tests, run mocha
```
mocha --watch
```

- jest: https://jestjs.io

### Run tests

`mocha tests-example/test.js`

