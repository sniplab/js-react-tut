import logo from './logo.svg';
import './App.css';
import React, {Component} from 'react';

class Button extends React.Component {
    constructor(props) {
        super(props);
        this.mymyClick = this.mymyClick.bind(this);
    }

    mymyClick() {
        // get FIRST element from page with class wrapper
        // and change its style
        document.getElementsByClassName('wrapper')[0].style.backgroundColor = "gray";

    }

    render() {
        return (
            <button onClick={this.mymyClick} className="clicker">
                Изменить дизайн
            </button>
        )
    }
}

export default Button;