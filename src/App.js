import logo from './logo.svg';
import './App.css';
import React, {Component} from 'react';

// let's create the new component that
// tracks the time and
// show specific message
function ShowBanner(props) {
    if (props.time > 45) {
        return (
            <div className="rest_block"> Rest! </div>
        )
    } else {
       return (
            <div className="work_block"> Work! </div>
        )
    }
}



class Clock extends React.Component {
  constructor(props) {
    // наследование у класса сверху (т.е. у React Component)
    super(props);
    // динамически изменяемый элемент
    this.state = {date: new Date()}
  }

  componentDidMount() {
      this.timerId = setInterval(
          // 1000 = 1sec
          // tick(): function/method below
          // !!!do not forget write tick as a function with ()!!!!
          () => this.tick(), 1000
      )
  }

  componentWillUnmount() {
      clearInterval(this.timerId)
  }

  tick() {
      this.setState({
        date: new Date()
      })
  }

  render() {
    return (
        <div>
            <ShowBanner time={this.state.date.getSeconds()} />
          <h1>Текущее время: {this.state.date.toLocaleTimeString()} </h1>
        </div>
    )
  }
}

export default Clock;
